import { CordialClientelNgrxPage } from './app.po';

describe('cordial-clientel-ngrx App', () => {
  let page: CordialClientelNgrxPage;

  beforeEach(() => {
    page = new CordialClientelNgrxPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
